<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.inkwell.internet.productregistration.service.PRHotelLocalServiceUtil"%>
<%@taglib uri="http://liferay.com/tld/portlet" 	prefix="liferay-portlet"%> 

<portlet:defineObjects />

<portlet:actionURL name="addHotel" var="addHotelURL" />

		<aui:form name="hotel" action="<%=addHotelURL%>" method="post">
			<aui:layout>
				<aui:column>
					<aui:input type="text" name="hotel" label="hotel" size="60" />
					<aui:input type="text" name="address" label="address" size="60" />
					<aui:input type="text" name="rooms" label="rooms" size="60">
						<aui:validator name="number"></aui:validator>
						<aui:validator name="minLength">1</aui:validator>
						<aui:validator name="maxLength">5</aui:validator>
					</aui:input>
					<aui:input type="text" name="country" label="country" size="60" />				
				</aui:column>
				<aui:button-row>
					<aui:button type="submit" name="Conferma" value="save" />
					<aui:button type="button" name="Annulla" value="cancel" last="true" />
				</aui:button-row>
			</aui:layout>
		</aui:form>