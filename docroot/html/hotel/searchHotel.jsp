<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.inkwell.internet.productregistration.service.PRHotelLocalServiceUtil"%>
<%@taglib uri="http://liferay.com/tld/portlet" 	prefix="liferay-portlet"%> 

<portlet:defineObjects />

 
		 <% 
		 String currentURL = PortalUtil.getCurrentURL(request);
		 %> 
 
		<liferay-ui:search-container delta="5" deltaConfigurable="true">
			
			<liferay-ui:search-container-results 
				results="<%=PRHotelLocalServiceUtil.getPRHotels(searchContainer.getStart(),searchContainer.getEnd())%>" 
				total="<%= PRHotelLocalServiceUtil.getPRHotelsCount() %>">
			</liferay-ui:search-container-results>	
			
			<liferay-ui:search-container-row className="com.inkwell.internet.productregistration.model.PRHotel" modelVar="aHotel" keyProperty="hotelId">
				
				<portlet:renderURL var="rowURL">
					<portlet:param name="backURL" value="<%=currentURL%>" />
					<portlet:param name="hotelId" value="<%=String.valueOf(aHotel.getHotelId())%>" />
					<portlet:param name="click" value="link" />
				</portlet:renderURL>
				
				<liferay-ui:search-container-column-text property="hotelId" name="hotelId" href="<%=rowURL%>" />
				<liferay-ui:search-container-column-text property="hotelName" name="hotel" />
				<liferay-ui:search-container-column-text property="adress" name="address" />
				<liferay-ui:search-container-column-text property="country" name="country" />
				<liferay-ui:search-container-column-text property="rooms" name="rooms" />
				<liferay-ui:search-container-column-text name="actions" >

					<liferay-ui:icon-menu>
					
						<!-- EDIT HOTEL -->
						<portlet:actionURL name="editHotel" var="editHotelURL">
							<portlet:param name="hotelId" value="<%=String.valueOf(aHotel.getHotelId())%>" />
						</portlet:actionURL>
						<liferay-ui:icon image="edit" message="editHotel" url="<%=editHotelURL.toString() %>" /> 
					 	
						<!-- DELETE HOTEL -->

						<portlet:actionURL name="deleteHotel" var="deleteHotelURL">
							<portlet:param name="hotelId" value="<%=String.valueOf(aHotel.getHotelId())%>" />
						</portlet:actionURL>
						<liferay-ui:icon image="delete" useDialog="deleteHotel" url="<%=deleteHotelURL.toString() %>" /> 
	
					 
					</liferay-ui:icon-menu>



				</liferay-ui:search-container-column-text>
				
				</liferay-ui:search-container-row>
			<liferay-ui:search-iterator />
		</liferay-ui:search-container> 