<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<portlet:renderURL var="homeURL"></portlet:renderURL>
<portlet:actionURL var="addEmployeeActionURL" windowState="normal" name="addEmployee"></portlet:actionURL>

<% if(SessionMessages.contains(renderRequest.getPortletSession(),"employee-add-success")){%>
<liferay-ui:success key="employee-add-success" message="Employee information have been added successfully." />
<%} %>

<% if(SessionErrors.contains(renderRequest.getPortletSession(),"employee-add-error")){%>
<liferay-ui:error key="employee-add-error" message="There is an Error occured while adding employee and please try again" />
<%} %>

<h2><liferay-ui:message key="addEmployee"/></h2>
<a href="<%=homeURL.toString() %>">Home</a><br/><br/>
<form action="<%=addEmployeeActionURL%>" name="employeeForm"  method="POST">
<b><liferay-ui:message key="employeeFirstName"/></b><br/>
<input type="text" name="<portlet:namespace/>employeeFirstName" id="<portlet:namespace/>employeeFirstName"/><br/>
<b><liferay-ui:message key="employeeLastName"/></b><br/>
<input type="text" name="<portlet:namespace/>employeeLastName" id="<portlet:namespace/>employeeLastName"/><br/>
<b><liferay-ui:message key="employeeDesignation"/></b><br/>
<input type="text" name="<portlet:namespace/>employeeDesignation" id="<portlet:namespace/>employeeDesignation"/><br/>
<b><liferay-ui:message key="employeeEmailAddress"/></b><br/>
<input type="text" name="<portlet:namespace/>emailAddress" id="<portlet:namespace/>emailAddress"/><br/>
<b><liferay-ui:message key="employeeAge"/></b><br/>
<input type="text" name="<portlet:namespace/>employeeAge" id="<portlet:namespace/>employeeAge"/><br/>
<b><liferay-ui:message key="employeePhoneNumber"/></b><br/>
<input type="text" name="<portlet:namespace/>phoneNumber" id="<portlet:namespace/>phoneNumber"/><br/>
<input type="submit" name="addStudent" id="addStudent" value="addEmployee"/>
</form>