<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<h1>ESEMPIO EXPANDO LIFERAY</h1>

<portlet:renderURL var="addEmployee">
<portlet:param name="mvcPath" value="/html/expando/add_employee.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="dislayEmployees">
<portlet:param name="mvcPath" value="/html/expando/display_employeed.jsp"/>
</portlet:renderURL>
 
<br/>
<a href="<%=addEmployee.toString()%>" ><liferay-ui:message key="addEmployee"/></a><br/>
<a href="<%=dislayEmployees.toString()%>"><liferay-ui:message key="displayEmployees"/></a><br/>