<%@page import="com.liferay.portlet.expando.NoSuchTableException"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoTable"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoRow"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.test.LiferayExpandoAPIAction"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoRowLocalServiceUtil"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:renderURL var="homeURL"></portlet:renderURL>
<h1><liferay-ui:message key="displayEmployee"/></h1>
<a href="<%=homeURL.toString() %>">Home</a><br/><br/>
<table style="width:100%" border="1">
<tr>
<th><liferay-ui:message key="employeeFirstName"/></th>
<th>LastName</th>
<th>Designation</th>
<th>EmailAddress</th>
<th>Age</th>
<th>PhoneNumber</th>
</tr>
<%
ExpandoTable expandoTable=null;
String message=null;
try {
expandoTable = ExpandoTableLocalServiceUtil.getTable(
themeDisplay.getCompanyId(), LiferayExpandoAPIAction.class.getName(),LiferayExpandoAPIAction.expandoTableName);
}
catch (NoSuchTableException nste) {
message="Table  not existed to show the data. please add data first and comeback to to see the data";
}
 
if(expandoTable!=null){
List<ExpandoRow> rows = ExpandoRowLocalServiceUtil.getRows(
themeDisplay.getCompanyId(), LiferayExpandoAPIAction.class.getName(),
LiferayExpandoAPIAction.expandoTableName, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
for (ExpandoRow row : rows) {
String data = ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[0], row.getClassPK(), StringPool.BLANK);
 
%>
<tr>
<td><%=ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[0], row.getClassPK(), StringPool.BLANK) %></td>
 
<td><%=ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[1], row.getClassPK(), StringPool.BLANK) %></td>
<td><%=ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[2], row.getClassPK(), StringPool.BLANK) %></td>
<td><%=ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[3], row.getClassPK(), StringPool.BLANK) %></td>
<td><%=ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[4], row.getClassPK(), 0) %></td>
<td><%=ExpandoValueLocalServiceUtil.getData(
themeDisplay.getCompanyId(),
LiferayExpandoAPIAction.class.getName(), LiferayExpandoAPIAction.expandoTableName,
LiferayExpandoAPIAction.columnNames[5], row.getClassPK(), StringPool.BLANK) %></td>
</tr>
<%}}%>
</table>
<h1><%=message!=null?message:StringPool.BLANK%></h1>