<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<portlet:defineObjects />


<portlet:actionURL name="chkControlla" var="chkContollaURL" />

<aui:form name="chkContolla" action="<%=chkContollaURL%>" method="post" >

<aui:model-context bean="${sessionScope.utente}" model="<%=com.liferay.portal.model.User.class%>" />

 <aui:fieldset label="Autentificazione">
 <aui:layout>
      <aui:column>
		
			<aui:input type="text" name="userId" label="user" size="60"/>
    		<aui:input type="password" name="password" id="password" value='' label="password" size="60" />
			<aui:input type="text" name="lastName" label="last-name" size="60"/>
			<aui:input type="text" name="firstName" label="first-name" size="60"/>
 			<aui:input required="true" type="text" name="emailAddress" label="email-address" size="60"/>
       		
 			<aui:select label="organization" name="azienda" >
				<aui:option label="" value="0" />
				<aui:option label="ThinkOpen" value="1" />
				<aui:option label="Amazon" value="2" />
				<aui:option label="Ducati" value="3" />
				<aui:option label="Ferrari" value="4" />
				<aui:option label="Lamborghini" value="5" />
			</aui:select>



	<jsp:useBean id="obj" class="com.test.TestOrganizzazioni" scope="page" />

				<aui:select name="organization" label="Organization">
					<c:forEach var="item" items="${obj.itemOrganization}">
						<aui:option>${item.name}</aui:option>
					</c:forEach>
				</aui:select>


			</aui:column>

       <aui:button-row>
			<aui:button type="submit" name="Conferma" value="save" />
			<aui:button type="button" name="Annulla" value="cancel" last="true"/>
      </aui:button-row>
 
   </aui:layout>
 </aui:fieldset>
</aui:form>
