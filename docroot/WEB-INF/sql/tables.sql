create table PR_PRHotel (
	hotelId LONG not null primary key,
	hotelName VARCHAR(75) null,
	adress VARCHAR(75) null,
	rooms LONG,
	country VARCHAR(75) null
);