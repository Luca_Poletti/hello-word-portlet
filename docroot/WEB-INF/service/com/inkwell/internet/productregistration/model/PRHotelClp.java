/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.internet.productregistration.model;

import com.inkwell.internet.productregistration.service.ClpSerializer;
import com.inkwell.internet.productregistration.service.PRHotelLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luca Poletti
 */
public class PRHotelClp extends BaseModelImpl<PRHotel> implements PRHotel {
	public PRHotelClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PRHotel.class;
	}

	@Override
	public String getModelClassName() {
		return PRHotel.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _hotelId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setHotelId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _hotelId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("hotelId", getHotelId());
		attributes.put("hotelName", getHotelName());
		attributes.put("adress", getAdress());
		attributes.put("rooms", getRooms());
		attributes.put("country", getCountry());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long hotelId = (Long)attributes.get("hotelId");

		if (hotelId != null) {
			setHotelId(hotelId);
		}

		String hotelName = (String)attributes.get("hotelName");

		if (hotelName != null) {
			setHotelName(hotelName);
		}

		String adress = (String)attributes.get("adress");

		if (adress != null) {
			setAdress(adress);
		}

		Long rooms = (Long)attributes.get("rooms");

		if (rooms != null) {
			setRooms(rooms);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}
	}

	@Override
	public long getHotelId() {
		return _hotelId;
	}

	@Override
	public void setHotelId(long hotelId) {
		_hotelId = hotelId;

		if (_prHotelRemoteModel != null) {
			try {
				Class<?> clazz = _prHotelRemoteModel.getClass();

				Method method = clazz.getMethod("setHotelId", long.class);

				method.invoke(_prHotelRemoteModel, hotelId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getHotelName() {
		return _hotelName;
	}

	@Override
	public void setHotelName(String hotelName) {
		_hotelName = hotelName;

		if (_prHotelRemoteModel != null) {
			try {
				Class<?> clazz = _prHotelRemoteModel.getClass();

				Method method = clazz.getMethod("setHotelName", String.class);

				method.invoke(_prHotelRemoteModel, hotelName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAdress() {
		return _adress;
	}

	@Override
	public void setAdress(String adress) {
		_adress = adress;

		if (_prHotelRemoteModel != null) {
			try {
				Class<?> clazz = _prHotelRemoteModel.getClass();

				Method method = clazz.getMethod("setAdress", String.class);

				method.invoke(_prHotelRemoteModel, adress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getRooms() {
		return _rooms;
	}

	@Override
	public void setRooms(long rooms) {
		_rooms = rooms;

		if (_prHotelRemoteModel != null) {
			try {
				Class<?> clazz = _prHotelRemoteModel.getClass();

				Method method = clazz.getMethod("setRooms", long.class);

				method.invoke(_prHotelRemoteModel, rooms);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCountry() {
		return _country;
	}

	@Override
	public void setCountry(String country) {
		_country = country;

		if (_prHotelRemoteModel != null) {
			try {
				Class<?> clazz = _prHotelRemoteModel.getClass();

				Method method = clazz.getMethod("setCountry", String.class);

				method.invoke(_prHotelRemoteModel, country);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPRHotelRemoteModel() {
		return _prHotelRemoteModel;
	}

	public void setPRHotelRemoteModel(BaseModel<?> prHotelRemoteModel) {
		_prHotelRemoteModel = prHotelRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _prHotelRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_prHotelRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PRHotelLocalServiceUtil.addPRHotel(this);
		}
		else {
			PRHotelLocalServiceUtil.updatePRHotel(this);
		}
	}

	@Override
	public PRHotel toEscapedModel() {
		return (PRHotel)ProxyUtil.newProxyInstance(PRHotel.class.getClassLoader(),
			new Class[] { PRHotel.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PRHotelClp clone = new PRHotelClp();

		clone.setHotelId(getHotelId());
		clone.setHotelName(getHotelName());
		clone.setAdress(getAdress());
		clone.setRooms(getRooms());
		clone.setCountry(getCountry());

		return clone;
	}

	@Override
	public int compareTo(PRHotel prHotel) {
		int value = 0;

		value = getHotelName().compareTo(prHotel.getHotelName());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PRHotelClp)) {
			return false;
		}

		PRHotelClp prHotel = (PRHotelClp)obj;

		long primaryKey = prHotel.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{hotelId=");
		sb.append(getHotelId());
		sb.append(", hotelName=");
		sb.append(getHotelName());
		sb.append(", adress=");
		sb.append(getAdress());
		sb.append(", rooms=");
		sb.append(getRooms());
		sb.append(", country=");
		sb.append(getCountry());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.inkwell.internet.productregistration.model.PRHotel");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>hotelId</column-name><column-value><![CDATA[");
		sb.append(getHotelId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>hotelName</column-name><column-value><![CDATA[");
		sb.append(getHotelName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>adress</column-name><column-value><![CDATA[");
		sb.append(getAdress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rooms</column-name><column-value><![CDATA[");
		sb.append(getRooms());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>country</column-name><column-value><![CDATA[");
		sb.append(getCountry());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _hotelId;
	private String _hotelName;
	private String _adress;
	private long _rooms;
	private String _country;
	private BaseModel<?> _prHotelRemoteModel;
	private Class<?> _clpSerializerClass = com.inkwell.internet.productregistration.service.ClpSerializer.class;
}