/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.internet.productregistration.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Luca Poletti
 * @generated
 */
public class PRHotelSoap implements Serializable {
	public static PRHotelSoap toSoapModel(PRHotel model) {
		PRHotelSoap soapModel = new PRHotelSoap();

		soapModel.setHotelId(model.getHotelId());
		soapModel.setHotelName(model.getHotelName());
		soapModel.setAdress(model.getAdress());
		soapModel.setRooms(model.getRooms());
		soapModel.setCountry(model.getCountry());

		return soapModel;
	}

	public static PRHotelSoap[] toSoapModels(PRHotel[] models) {
		PRHotelSoap[] soapModels = new PRHotelSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PRHotelSoap[][] toSoapModels(PRHotel[][] models) {
		PRHotelSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PRHotelSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PRHotelSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PRHotelSoap[] toSoapModels(List<PRHotel> models) {
		List<PRHotelSoap> soapModels = new ArrayList<PRHotelSoap>(models.size());

		for (PRHotel model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PRHotelSoap[soapModels.size()]);
	}

	public PRHotelSoap() {
	}

	public long getPrimaryKey() {
		return _hotelId;
	}

	public void setPrimaryKey(long pk) {
		setHotelId(pk);
	}

	public long getHotelId() {
		return _hotelId;
	}

	public void setHotelId(long hotelId) {
		_hotelId = hotelId;
	}

	public String getHotelName() {
		return _hotelName;
	}

	public void setHotelName(String hotelName) {
		_hotelName = hotelName;
	}

	public String getAdress() {
		return _adress;
	}

	public void setAdress(String adress) {
		_adress = adress;
	}

	public long getRooms() {
		return _rooms;
	}

	public void setRooms(long rooms) {
		_rooms = rooms;
	}

	public String getCountry() {
		return _country;
	}

	public void setCountry(String country) {
		_country = country;
	}

	private long _hotelId;
	private String _hotelName;
	private String _adress;
	private long _rooms;
	private String _country;
}