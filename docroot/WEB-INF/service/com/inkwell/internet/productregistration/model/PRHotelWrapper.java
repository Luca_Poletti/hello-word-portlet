/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.internet.productregistration.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PRHotel}.
 * </p>
 *
 * @author Luca Poletti
 * @see PRHotel
 * @generated
 */
public class PRHotelWrapper implements PRHotel, ModelWrapper<PRHotel> {
	public PRHotelWrapper(PRHotel prHotel) {
		_prHotel = prHotel;
	}

	@Override
	public Class<?> getModelClass() {
		return PRHotel.class;
	}

	@Override
	public String getModelClassName() {
		return PRHotel.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("hotelId", getHotelId());
		attributes.put("hotelName", getHotelName());
		attributes.put("adress", getAdress());
		attributes.put("rooms", getRooms());
		attributes.put("country", getCountry());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long hotelId = (Long)attributes.get("hotelId");

		if (hotelId != null) {
			setHotelId(hotelId);
		}

		String hotelName = (String)attributes.get("hotelName");

		if (hotelName != null) {
			setHotelName(hotelName);
		}

		String adress = (String)attributes.get("adress");

		if (adress != null) {
			setAdress(adress);
		}

		Long rooms = (Long)attributes.get("rooms");

		if (rooms != null) {
			setRooms(rooms);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}
	}

	/**
	* Returns the primary key of this p r hotel.
	*
	* @return the primary key of this p r hotel
	*/
	@Override
	public long getPrimaryKey() {
		return _prHotel.getPrimaryKey();
	}

	/**
	* Sets the primary key of this p r hotel.
	*
	* @param primaryKey the primary key of this p r hotel
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_prHotel.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the hotel ID of this p r hotel.
	*
	* @return the hotel ID of this p r hotel
	*/
	@Override
	public long getHotelId() {
		return _prHotel.getHotelId();
	}

	/**
	* Sets the hotel ID of this p r hotel.
	*
	* @param hotelId the hotel ID of this p r hotel
	*/
	@Override
	public void setHotelId(long hotelId) {
		_prHotel.setHotelId(hotelId);
	}

	/**
	* Returns the hotel name of this p r hotel.
	*
	* @return the hotel name of this p r hotel
	*/
	@Override
	public java.lang.String getHotelName() {
		return _prHotel.getHotelName();
	}

	/**
	* Sets the hotel name of this p r hotel.
	*
	* @param hotelName the hotel name of this p r hotel
	*/
	@Override
	public void setHotelName(java.lang.String hotelName) {
		_prHotel.setHotelName(hotelName);
	}

	/**
	* Returns the adress of this p r hotel.
	*
	* @return the adress of this p r hotel
	*/
	@Override
	public java.lang.String getAdress() {
		return _prHotel.getAdress();
	}

	/**
	* Sets the adress of this p r hotel.
	*
	* @param adress the adress of this p r hotel
	*/
	@Override
	public void setAdress(java.lang.String adress) {
		_prHotel.setAdress(adress);
	}

	/**
	* Returns the rooms of this p r hotel.
	*
	* @return the rooms of this p r hotel
	*/
	@Override
	public long getRooms() {
		return _prHotel.getRooms();
	}

	/**
	* Sets the rooms of this p r hotel.
	*
	* @param rooms the rooms of this p r hotel
	*/
	@Override
	public void setRooms(long rooms) {
		_prHotel.setRooms(rooms);
	}

	/**
	* Returns the country of this p r hotel.
	*
	* @return the country of this p r hotel
	*/
	@Override
	public java.lang.String getCountry() {
		return _prHotel.getCountry();
	}

	/**
	* Sets the country of this p r hotel.
	*
	* @param country the country of this p r hotel
	*/
	@Override
	public void setCountry(java.lang.String country) {
		_prHotel.setCountry(country);
	}

	@Override
	public boolean isNew() {
		return _prHotel.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_prHotel.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _prHotel.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_prHotel.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _prHotel.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _prHotel.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_prHotel.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _prHotel.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_prHotel.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_prHotel.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_prHotel.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PRHotelWrapper((PRHotel)_prHotel.clone());
	}

	@Override
	public int compareTo(
		com.inkwell.internet.productregistration.model.PRHotel prHotel) {
		return _prHotel.compareTo(prHotel);
	}

	@Override
	public int hashCode() {
		return _prHotel.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.inkwell.internet.productregistration.model.PRHotel> toCacheModel() {
		return _prHotel.toCacheModel();
	}

	@Override
	public com.inkwell.internet.productregistration.model.PRHotel toEscapedModel() {
		return new PRHotelWrapper(_prHotel.toEscapedModel());
	}

	@Override
	public com.inkwell.internet.productregistration.model.PRHotel toUnescapedModel() {
		return new PRHotelWrapper(_prHotel.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _prHotel.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _prHotel.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_prHotel.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PRHotelWrapper)) {
			return false;
		}

		PRHotelWrapper prHotelWrapper = (PRHotelWrapper)obj;

		if (Validator.equals(_prHotel, prHotelWrapper._prHotel)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PRHotel getWrappedPRHotel() {
		return _prHotel;
	}

	@Override
	public PRHotel getWrappedModel() {
		return _prHotel;
	}

	@Override
	public void resetOriginalValues() {
		_prHotel.resetOriginalValues();
	}

	private PRHotel _prHotel;
}