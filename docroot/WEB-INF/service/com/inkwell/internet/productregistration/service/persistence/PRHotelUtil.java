/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.internet.productregistration.service.persistence;

import com.inkwell.internet.productregistration.model.PRHotel;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the p r hotel service. This utility wraps {@link PRHotelPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luca Poletti
 * @see PRHotelPersistence
 * @see PRHotelPersistenceImpl
 * @generated
 */
public class PRHotelUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PRHotel prHotel) {
		getPersistence().clearCache(prHotel);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PRHotel> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PRHotel> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PRHotel> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static PRHotel update(PRHotel prHotel) throws SystemException {
		return getPersistence().update(prHotel);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static PRHotel update(PRHotel prHotel, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(prHotel, serviceContext);
	}

	/**
	* Returns all the p r hotels where hotelId = &#63; and hotelName = &#63;.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @return the matching p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.inkwell.internet.productregistration.model.PRHotel> findByG_PN(
		long hotelId, java.lang.String hotelName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByG_PN(hotelId, hotelName);
	}

	/**
	* Returns a range of all the p r hotels where hotelId = &#63; and hotelName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @param start the lower bound of the range of p r hotels
	* @param end the upper bound of the range of p r hotels (not inclusive)
	* @return the range of matching p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.inkwell.internet.productregistration.model.PRHotel> findByG_PN(
		long hotelId, java.lang.String hotelName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByG_PN(hotelId, hotelName, start, end);
	}

	/**
	* Returns an ordered range of all the p r hotels where hotelId = &#63; and hotelName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @param start the lower bound of the range of p r hotels
	* @param end the upper bound of the range of p r hotels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.inkwell.internet.productregistration.model.PRHotel> findByG_PN(
		long hotelId, java.lang.String hotelName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByG_PN(hotelId, hotelName, start, end, orderByComparator);
	}

	/**
	* Returns the first p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching p r hotel
	* @throws com.inkwell.internet.productregistration.NoSuchHotelException if a matching p r hotel could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel findByG_PN_First(
		long hotelId, java.lang.String hotelName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.inkwell.internet.productregistration.NoSuchHotelException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByG_PN_First(hotelId, hotelName, orderByComparator);
	}

	/**
	* Returns the first p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching p r hotel, or <code>null</code> if a matching p r hotel could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel fetchByG_PN_First(
		long hotelId, java.lang.String hotelName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByG_PN_First(hotelId, hotelName, orderByComparator);
	}

	/**
	* Returns the last p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching p r hotel
	* @throws com.inkwell.internet.productregistration.NoSuchHotelException if a matching p r hotel could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel findByG_PN_Last(
		long hotelId, java.lang.String hotelName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.inkwell.internet.productregistration.NoSuchHotelException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByG_PN_Last(hotelId, hotelName, orderByComparator);
	}

	/**
	* Returns the last p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching p r hotel, or <code>null</code> if a matching p r hotel could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel fetchByG_PN_Last(
		long hotelId, java.lang.String hotelName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByG_PN_Last(hotelId, hotelName, orderByComparator);
	}

	/**
	* Removes all the p r hotels where hotelId = &#63; and hotelName = &#63; from the database.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByG_PN(long hotelId, java.lang.String hotelName)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByG_PN(hotelId, hotelName);
	}

	/**
	* Returns the number of p r hotels where hotelId = &#63; and hotelName = &#63;.
	*
	* @param hotelId the hotel ID
	* @param hotelName the hotel name
	* @return the number of matching p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static int countByG_PN(long hotelId, java.lang.String hotelName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByG_PN(hotelId, hotelName);
	}

	/**
	* Caches the p r hotel in the entity cache if it is enabled.
	*
	* @param prHotel the p r hotel
	*/
	public static void cacheResult(
		com.inkwell.internet.productregistration.model.PRHotel prHotel) {
		getPersistence().cacheResult(prHotel);
	}

	/**
	* Caches the p r hotels in the entity cache if it is enabled.
	*
	* @param prHotels the p r hotels
	*/
	public static void cacheResult(
		java.util.List<com.inkwell.internet.productregistration.model.PRHotel> prHotels) {
		getPersistence().cacheResult(prHotels);
	}

	/**
	* Creates a new p r hotel with the primary key. Does not add the p r hotel to the database.
	*
	* @param hotelId the primary key for the new p r hotel
	* @return the new p r hotel
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel create(
		long hotelId) {
		return getPersistence().create(hotelId);
	}

	/**
	* Removes the p r hotel with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param hotelId the primary key of the p r hotel
	* @return the p r hotel that was removed
	* @throws com.inkwell.internet.productregistration.NoSuchHotelException if a p r hotel with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel remove(
		long hotelId)
		throws com.inkwell.internet.productregistration.NoSuchHotelException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(hotelId);
	}

	public static com.inkwell.internet.productregistration.model.PRHotel updateImpl(
		com.inkwell.internet.productregistration.model.PRHotel prHotel)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(prHotel);
	}

	/**
	* Returns the p r hotel with the primary key or throws a {@link com.inkwell.internet.productregistration.NoSuchHotelException} if it could not be found.
	*
	* @param hotelId the primary key of the p r hotel
	* @return the p r hotel
	* @throws com.inkwell.internet.productregistration.NoSuchHotelException if a p r hotel with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel findByPrimaryKey(
		long hotelId)
		throws com.inkwell.internet.productregistration.NoSuchHotelException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(hotelId);
	}

	/**
	* Returns the p r hotel with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param hotelId the primary key of the p r hotel
	* @return the p r hotel, or <code>null</code> if a p r hotel with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.inkwell.internet.productregistration.model.PRHotel fetchByPrimaryKey(
		long hotelId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(hotelId);
	}

	/**
	* Returns all the p r hotels.
	*
	* @return the p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.inkwell.internet.productregistration.model.PRHotel> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the p r hotels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of p r hotels
	* @param end the upper bound of the range of p r hotels (not inclusive)
	* @return the range of p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.inkwell.internet.productregistration.model.PRHotel> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the p r hotels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of p r hotels
	* @param end the upper bound of the range of p r hotels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.inkwell.internet.productregistration.model.PRHotel> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the p r hotels from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of p r hotels.
	*
	* @return the number of p r hotels
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PRHotelPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PRHotelPersistence)PortletBeanLocatorUtil.locate(com.inkwell.internet.productregistration.service.ClpSerializer.getServletContextName(),
					PRHotelPersistence.class.getName());

			ReferenceRegistry.registerReference(PRHotelUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(PRHotelPersistence persistence) {
	}

	private static PRHotelPersistence _persistence;
}