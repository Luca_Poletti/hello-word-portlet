/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.internet.productregistration.service.persistence;

import com.inkwell.internet.productregistration.NoSuchHotelException;
import com.inkwell.internet.productregistration.model.PRHotel;
import com.inkwell.internet.productregistration.model.impl.PRHotelImpl;
import com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the p r hotel service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luca Poletti
 * @see PRHotelPersistence
 * @see PRHotelUtil
 * @generated
 */
public class PRHotelPersistenceImpl extends BasePersistenceImpl<PRHotel>
	implements PRHotelPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PRHotelUtil} to access the p r hotel persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PRHotelImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelModelImpl.FINDER_CACHE_ENABLED, PRHotelImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelModelImpl.FINDER_CACHE_ENABLED, PRHotelImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_G_PN = new FinderPath(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelModelImpl.FINDER_CACHE_ENABLED, PRHotelImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByG_PN",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_PN = new FinderPath(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelModelImpl.FINDER_CACHE_ENABLED, PRHotelImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByG_PN",
			new String[] { Long.class.getName(), String.class.getName() },
			PRHotelModelImpl.HOTELID_COLUMN_BITMASK |
			PRHotelModelImpl.HOTELNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_G_PN = new FinderPath(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByG_PN",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the p r hotels where hotelId = &#63; and hotelName = &#63;.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @return the matching p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PRHotel> findByG_PN(long hotelId, String hotelName)
		throws SystemException {
		return findByG_PN(hotelId, hotelName, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the p r hotels where hotelId = &#63; and hotelName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @param start the lower bound of the range of p r hotels
	 * @param end the upper bound of the range of p r hotels (not inclusive)
	 * @return the range of matching p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PRHotel> findByG_PN(long hotelId, String hotelName, int start,
		int end) throws SystemException {
		return findByG_PN(hotelId, hotelName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the p r hotels where hotelId = &#63; and hotelName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @param start the lower bound of the range of p r hotels
	 * @param end the upper bound of the range of p r hotels (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PRHotel> findByG_PN(long hotelId, String hotelName, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_PN;
			finderArgs = new Object[] { hotelId, hotelName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_G_PN;
			finderArgs = new Object[] {
					hotelId, hotelName,
					
					start, end, orderByComparator
				};
		}

		List<PRHotel> list = (List<PRHotel>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PRHotel prHotel : list) {
				if ((hotelId != prHotel.getHotelId()) ||
						!Validator.equals(hotelName, prHotel.getHotelName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PRHOTEL_WHERE);

			query.append(_FINDER_COLUMN_G_PN_HOTELID_2);

			boolean bindHotelName = false;

			if (hotelName == null) {
				query.append(_FINDER_COLUMN_G_PN_HOTELNAME_1);
			}
			else if (hotelName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_PN_HOTELNAME_3);
			}
			else {
				bindHotelName = true;

				query.append(_FINDER_COLUMN_G_PN_HOTELNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PRHotelModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(hotelId);

				if (bindHotelName) {
					qPos.add(hotelName);
				}

				if (!pagination) {
					list = (List<PRHotel>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PRHotel>(list);
				}
				else {
					list = (List<PRHotel>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching p r hotel
	 * @throws com.inkwell.internet.productregistration.NoSuchHotelException if a matching p r hotel could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel findByG_PN_First(long hotelId, String hotelName,
		OrderByComparator orderByComparator)
		throws NoSuchHotelException, SystemException {
		PRHotel prHotel = fetchByG_PN_First(hotelId, hotelName,
				orderByComparator);

		if (prHotel != null) {
			return prHotel;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("hotelId=");
		msg.append(hotelId);

		msg.append(", hotelName=");
		msg.append(hotelName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchHotelException(msg.toString());
	}

	/**
	 * Returns the first p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching p r hotel, or <code>null</code> if a matching p r hotel could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel fetchByG_PN_First(long hotelId, String hotelName,
		OrderByComparator orderByComparator) throws SystemException {
		List<PRHotel> list = findByG_PN(hotelId, hotelName, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching p r hotel
	 * @throws com.inkwell.internet.productregistration.NoSuchHotelException if a matching p r hotel could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel findByG_PN_Last(long hotelId, String hotelName,
		OrderByComparator orderByComparator)
		throws NoSuchHotelException, SystemException {
		PRHotel prHotel = fetchByG_PN_Last(hotelId, hotelName, orderByComparator);

		if (prHotel != null) {
			return prHotel;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("hotelId=");
		msg.append(hotelId);

		msg.append(", hotelName=");
		msg.append(hotelName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchHotelException(msg.toString());
	}

	/**
	 * Returns the last p r hotel in the ordered set where hotelId = &#63; and hotelName = &#63;.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching p r hotel, or <code>null</code> if a matching p r hotel could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel fetchByG_PN_Last(long hotelId, String hotelName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByG_PN(hotelId, hotelName);

		if (count == 0) {
			return null;
		}

		List<PRHotel> list = findByG_PN(hotelId, hotelName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the p r hotels where hotelId = &#63; and hotelName = &#63; from the database.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByG_PN(long hotelId, String hotelName)
		throws SystemException {
		for (PRHotel prHotel : findByG_PN(hotelId, hotelName,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(prHotel);
		}
	}

	/**
	 * Returns the number of p r hotels where hotelId = &#63; and hotelName = &#63;.
	 *
	 * @param hotelId the hotel ID
	 * @param hotelName the hotel name
	 * @return the number of matching p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByG_PN(long hotelId, String hotelName)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_G_PN;

		Object[] finderArgs = new Object[] { hotelId, hotelName };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PRHOTEL_WHERE);

			query.append(_FINDER_COLUMN_G_PN_HOTELID_2);

			boolean bindHotelName = false;

			if (hotelName == null) {
				query.append(_FINDER_COLUMN_G_PN_HOTELNAME_1);
			}
			else if (hotelName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_PN_HOTELNAME_3);
			}
			else {
				bindHotelName = true;

				query.append(_FINDER_COLUMN_G_PN_HOTELNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(hotelId);

				if (bindHotelName) {
					qPos.add(hotelName);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_PN_HOTELID_2 = "prHotel.hotelId = ? AND ";
	private static final String _FINDER_COLUMN_G_PN_HOTELNAME_1 = "prHotel.hotelName IS NULL";
	private static final String _FINDER_COLUMN_G_PN_HOTELNAME_2 = "prHotel.hotelName = ?";
	private static final String _FINDER_COLUMN_G_PN_HOTELNAME_3 = "(prHotel.hotelName IS NULL OR prHotel.hotelName = '')";

	public PRHotelPersistenceImpl() {
		setModelClass(PRHotel.class);
	}

	/**
	 * Caches the p r hotel in the entity cache if it is enabled.
	 *
	 * @param prHotel the p r hotel
	 */
	@Override
	public void cacheResult(PRHotel prHotel) {
		EntityCacheUtil.putResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelImpl.class, prHotel.getPrimaryKey(), prHotel);

		prHotel.resetOriginalValues();
	}

	/**
	 * Caches the p r hotels in the entity cache if it is enabled.
	 *
	 * @param prHotels the p r hotels
	 */
	@Override
	public void cacheResult(List<PRHotel> prHotels) {
		for (PRHotel prHotel : prHotels) {
			if (EntityCacheUtil.getResult(
						PRHotelModelImpl.ENTITY_CACHE_ENABLED,
						PRHotelImpl.class, prHotel.getPrimaryKey()) == null) {
				cacheResult(prHotel);
			}
			else {
				prHotel.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all p r hotels.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PRHotelImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PRHotelImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the p r hotel.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PRHotel prHotel) {
		EntityCacheUtil.removeResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelImpl.class, prHotel.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<PRHotel> prHotels) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PRHotel prHotel : prHotels) {
			EntityCacheUtil.removeResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
				PRHotelImpl.class, prHotel.getPrimaryKey());
		}
	}

	/**
	 * Creates a new p r hotel with the primary key. Does not add the p r hotel to the database.
	 *
	 * @param hotelId the primary key for the new p r hotel
	 * @return the new p r hotel
	 */
	@Override
	public PRHotel create(long hotelId) {
		PRHotel prHotel = new PRHotelImpl();

		prHotel.setNew(true);
		prHotel.setPrimaryKey(hotelId);

		return prHotel;
	}

	/**
	 * Removes the p r hotel with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param hotelId the primary key of the p r hotel
	 * @return the p r hotel that was removed
	 * @throws com.inkwell.internet.productregistration.NoSuchHotelException if a p r hotel with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel remove(long hotelId)
		throws NoSuchHotelException, SystemException {
		return remove((Serializable)hotelId);
	}

	/**
	 * Removes the p r hotel with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the p r hotel
	 * @return the p r hotel that was removed
	 * @throws com.inkwell.internet.productregistration.NoSuchHotelException if a p r hotel with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel remove(Serializable primaryKey)
		throws NoSuchHotelException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PRHotel prHotel = (PRHotel)session.get(PRHotelImpl.class, primaryKey);

			if (prHotel == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchHotelException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(prHotel);
		}
		catch (NoSuchHotelException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PRHotel removeImpl(PRHotel prHotel) throws SystemException {
		prHotel = toUnwrappedModel(prHotel);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(prHotel)) {
				prHotel = (PRHotel)session.get(PRHotelImpl.class,
						prHotel.getPrimaryKeyObj());
			}

			if (prHotel != null) {
				session.delete(prHotel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (prHotel != null) {
			clearCache(prHotel);
		}

		return prHotel;
	}

	@Override
	public PRHotel updateImpl(
		com.inkwell.internet.productregistration.model.PRHotel prHotel)
		throws SystemException {
		prHotel = toUnwrappedModel(prHotel);

		boolean isNew = prHotel.isNew();

		PRHotelModelImpl prHotelModelImpl = (PRHotelModelImpl)prHotel;

		Session session = null;

		try {
			session = openSession();

			if (prHotel.isNew()) {
				session.save(prHotel);

				prHotel.setNew(false);
			}
			else {
				session.merge(prHotel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PRHotelModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((prHotelModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_PN.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						prHotelModelImpl.getOriginalHotelId(),
						prHotelModelImpl.getOriginalHotelName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_PN, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_PN,
					args);

				args = new Object[] {
						prHotelModelImpl.getHotelId(),
						prHotelModelImpl.getHotelName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_PN, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_PN,
					args);
			}
		}

		EntityCacheUtil.putResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
			PRHotelImpl.class, prHotel.getPrimaryKey(), prHotel);

		return prHotel;
	}

	protected PRHotel toUnwrappedModel(PRHotel prHotel) {
		if (prHotel instanceof PRHotelImpl) {
			return prHotel;
		}

		PRHotelImpl prHotelImpl = new PRHotelImpl();

		prHotelImpl.setNew(prHotel.isNew());
		prHotelImpl.setPrimaryKey(prHotel.getPrimaryKey());

		prHotelImpl.setHotelId(prHotel.getHotelId());
		prHotelImpl.setHotelName(prHotel.getHotelName());
		prHotelImpl.setAdress(prHotel.getAdress());
		prHotelImpl.setRooms(prHotel.getRooms());
		prHotelImpl.setCountry(prHotel.getCountry());

		return prHotelImpl;
	}

	/**
	 * Returns the p r hotel with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the p r hotel
	 * @return the p r hotel
	 * @throws com.inkwell.internet.productregistration.NoSuchHotelException if a p r hotel with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel findByPrimaryKey(Serializable primaryKey)
		throws NoSuchHotelException, SystemException {
		PRHotel prHotel = fetchByPrimaryKey(primaryKey);

		if (prHotel == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchHotelException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return prHotel;
	}

	/**
	 * Returns the p r hotel with the primary key or throws a {@link com.inkwell.internet.productregistration.NoSuchHotelException} if it could not be found.
	 *
	 * @param hotelId the primary key of the p r hotel
	 * @return the p r hotel
	 * @throws com.inkwell.internet.productregistration.NoSuchHotelException if a p r hotel with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel findByPrimaryKey(long hotelId)
		throws NoSuchHotelException, SystemException {
		return findByPrimaryKey((Serializable)hotelId);
	}

	/**
	 * Returns the p r hotel with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the p r hotel
	 * @return the p r hotel, or <code>null</code> if a p r hotel with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PRHotel prHotel = (PRHotel)EntityCacheUtil.getResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
				PRHotelImpl.class, primaryKey);

		if (prHotel == _nullPRHotel) {
			return null;
		}

		if (prHotel == null) {
			Session session = null;

			try {
				session = openSession();

				prHotel = (PRHotel)session.get(PRHotelImpl.class, primaryKey);

				if (prHotel != null) {
					cacheResult(prHotel);
				}
				else {
					EntityCacheUtil.putResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
						PRHotelImpl.class, primaryKey, _nullPRHotel);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PRHotelModelImpl.ENTITY_CACHE_ENABLED,
					PRHotelImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return prHotel;
	}

	/**
	 * Returns the p r hotel with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param hotelId the primary key of the p r hotel
	 * @return the p r hotel, or <code>null</code> if a p r hotel with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PRHotel fetchByPrimaryKey(long hotelId) throws SystemException {
		return fetchByPrimaryKey((Serializable)hotelId);
	}

	/**
	 * Returns all the p r hotels.
	 *
	 * @return the p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PRHotel> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the p r hotels.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of p r hotels
	 * @param end the upper bound of the range of p r hotels (not inclusive)
	 * @return the range of p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PRHotel> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the p r hotels.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.inkwell.internet.productregistration.model.impl.PRHotelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of p r hotels
	 * @param end the upper bound of the range of p r hotels (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PRHotel> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PRHotel> list = (List<PRHotel>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRHOTEL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRHOTEL;

				if (pagination) {
					sql = sql.concat(PRHotelModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PRHotel>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PRHotel>(list);
				}
				else {
					list = (List<PRHotel>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the p r hotels from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PRHotel prHotel : findAll()) {
			remove(prHotel);
		}
	}

	/**
	 * Returns the number of p r hotels.
	 *
	 * @return the number of p r hotels
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRHOTEL);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the p r hotel persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.inkwell.internet.productregistration.model.PRHotel")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PRHotel>> listenersList = new ArrayList<ModelListener<PRHotel>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PRHotel>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PRHotelImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PRHOTEL = "SELECT prHotel FROM PRHotel prHotel";
	private static final String _SQL_SELECT_PRHOTEL_WHERE = "SELECT prHotel FROM PRHotel prHotel WHERE ";
	private static final String _SQL_COUNT_PRHOTEL = "SELECT COUNT(prHotel) FROM PRHotel prHotel";
	private static final String _SQL_COUNT_PRHOTEL_WHERE = "SELECT COUNT(prHotel) FROM PRHotel prHotel WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "prHotel.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PRHotel exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PRHotel exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PRHotelPersistenceImpl.class);
	private static PRHotel _nullPRHotel = new PRHotelImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PRHotel> toCacheModel() {
				return _nullPRHotelCacheModel;
			}
		};

	private static CacheModel<PRHotel> _nullPRHotelCacheModel = new CacheModel<PRHotel>() {
			@Override
			public PRHotel toEntityModel() {
				return _nullPRHotel;
			}
		};
}