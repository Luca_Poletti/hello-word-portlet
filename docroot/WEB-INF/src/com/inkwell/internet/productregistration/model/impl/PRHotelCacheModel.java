/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.internet.productregistration.model.impl;

import com.inkwell.internet.productregistration.model.PRHotel;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PRHotel in entity cache.
 *
 * @author Luca Poletti
 * @see PRHotel
 * @generated
 */
public class PRHotelCacheModel implements CacheModel<PRHotel>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{hotelId=");
		sb.append(hotelId);
		sb.append(", hotelName=");
		sb.append(hotelName);
		sb.append(", adress=");
		sb.append(adress);
		sb.append(", rooms=");
		sb.append(rooms);
		sb.append(", country=");
		sb.append(country);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PRHotel toEntityModel() {
		PRHotelImpl prHotelImpl = new PRHotelImpl();

		prHotelImpl.setHotelId(hotelId);

		if (hotelName == null) {
			prHotelImpl.setHotelName(StringPool.BLANK);
		}
		else {
			prHotelImpl.setHotelName(hotelName);
		}

		if (adress == null) {
			prHotelImpl.setAdress(StringPool.BLANK);
		}
		else {
			prHotelImpl.setAdress(adress);
		}

		prHotelImpl.setRooms(rooms);

		if (country == null) {
			prHotelImpl.setCountry(StringPool.BLANK);
		}
		else {
			prHotelImpl.setCountry(country);
		}

		prHotelImpl.resetOriginalValues();

		return prHotelImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		hotelId = objectInput.readLong();
		hotelName = objectInput.readUTF();
		adress = objectInput.readUTF();
		rooms = objectInput.readLong();
		country = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(hotelId);

		if (hotelName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(hotelName);
		}

		if (adress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(adress);
		}

		objectOutput.writeLong(rooms);

		if (country == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(country);
		}
	}

	public long hotelId;
	public String hotelName;
	public String adress;
	public long rooms;
	public String country;
}