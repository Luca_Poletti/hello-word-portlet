package com.test;

import java.util.List;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;


public class TestOrganizzazioni {
	
	//Caricamento lista Organizzazioni
	public List<Organization> getItemOrganization() 
	{
	try {
		
		 List<Organization> organizationList = OrganizationLocalServiceUtil.getOrganizations(0, OrganizationLocalServiceUtil.getOrganizationsCount());
		return organizationList;
		
		}
	catch(Exception e)	
	{
		System.out.println(e);
	}
	return null;
	}

}
