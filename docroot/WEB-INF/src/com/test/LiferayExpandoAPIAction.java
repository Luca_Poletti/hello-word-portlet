package com.test;

import java.io.IOException;
import java.util.ArrayList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.expando.NoSuchTableException;
import com.liferay.portlet.expando.model.ExpandoColumnConstants;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class LiferayExpandoAPIAction extends MVCPortlet {

	public static String[] columnNames = { "FirstName", "LastName",
			"Designation", "EmailAddress", "Age", "PhoneNumber" };

	public static String expandoTableName = "Employee";
	private static Log _log = LogFactoryUtil
			.getLog(LiferayExpandoAPIAction.class);
	private ArrayList<Column> columns = new ArrayList<Column>();

	public void addEmployee(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		try {
	
			columns.add(new Column(0,"FirstName",1));
			columns.add(new Column(1,"LastName",1));
			columns.add(new Column(2,"Designation",1));
			columns.add(new Column(3,"EmailAddress",1));
			columns.add(new Column(4,"Age",2));
			columns.add(new Column(5,"PhoneNumber",1));		
			columns.get(0).getCol_Name();
			columns.get(1).getCol_Name();
			columns.get(2).getCol_Name();
			
			boolean dataAdedSuccess = false;
			String firstNameValue = ParamUtil.getString(actionRequest,
					"employeeFirstName");
			String lastNameValue = ParamUtil.getString(actionRequest,
					"employeeLastName");
			String employeeDesignation = ParamUtil.getString(actionRequest,
					"employeeDesignation");
			String emailAddressValue = ParamUtil.getString(actionRequest,
					"emailAddress");
			int employeeAgeValue = ParamUtil.getInteger(actionRequest,
					"employeeAge");
			String phoneNumberValue = ParamUtil.getString(actionRequest,
					"phoneNumber");
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
					.getAttribute(WebKeys.THEME_DISPLAY);
			long companyId = themeDisplay.getCompanyId();

			ExpandoTable expandoTable = checkTable(companyId, expandoTableName);
			_log.info("expandoTable" + expandoTable.getTableId());
			if (expandoTable != null) {
				long classPK = CounterLocalServiceUtil
						.increment(LiferayExpandoAPIAction.class.getName());
				ExpandoValueLocalServiceUtil.addValue(companyId,
						LiferayExpandoAPIAction.class.getName(),
						expandoTableName, columnNames[0], classPK,
						firstNameValue);
				ExpandoValueLocalServiceUtil.addValue(companyId,
						LiferayExpandoAPIAction.class.getName(),
						expandoTableName, columnNames[1], classPK,
						lastNameValue);
				ExpandoValueLocalServiceUtil.addValue(companyId,
						LiferayExpandoAPIAction.class.getName(),
						expandoTableName, columnNames[2], classPK,
						employeeDesignation);
				ExpandoValueLocalServiceUtil.addValue(companyId,
						LiferayExpandoAPIAction.class.getName(),
						expandoTableName, columnNames[3], classPK,
						emailAddressValue);
				ExpandoValueLocalServiceUtil.addValue(companyId,
						LiferayExpandoAPIAction.class.getName(),
						expandoTableName, columnNames[4], classPK,
						employeeAgeValue);
				ExpandoValueLocalServiceUtil.addValue(companyId,
						LiferayExpandoAPIAction.class.getName(),
						expandoTableName, columnNames[5], classPK,
						phoneNumberValue);
				dataAdedSuccess = true;
			}

			if (dataAdedSuccess) {
				// adding success message
				SessionMessages.add(actionRequest.getPortletSession(),
						"employee-add-success");
				_log.info("Employee have been added successfylly");
			}
			// navigate to add student jsp page
			actionResponse.setRenderParameter("mvcPath",
					"/html/expando/add_employee.jsp");
		} catch (Exception e) {
			SessionErrors.add(actionRequest.getPortletSession(),
					"employee-add-error");
			e.printStackTrace();
		}
	}

	public ExpandoTable addTable(long companyId, String tableName)
			throws PortalException, SystemException {
		ExpandoTable expandoTable = ExpandoTableLocalServiceUtil.addTable(
				companyId, LiferayExpandoAPIAction.class.getName(), tableName);
		_log.error("Expando Table Created Successfully.");
		return expandoTable;
	}

	public ExpandoTable checkTable(long companyId, String tableName)
			throws Exception {

		ExpandoTable expandoTable = null;

		try {
			expandoTable = ExpandoTableLocalServiceUtil.getTable(companyId,
					LiferayExpandoAPIAction.class.getName(), tableName);
		} catch (NoSuchTableException nste) {
			expandoTable = addTable(companyId, tableName);
			for (String columnName : columnNames) {
				if (columnName.equals("Age")) {
					ExpandoColumnLocalServiceUtil.addColumn(
							expandoTable.getTableId(), columnName,
							ExpandoColumnConstants.INTEGER);
				} else {
					ExpandoColumnLocalServiceUtil.addColumn(
							expandoTable.getTableId(), columnName,
							ExpandoColumnConstants.STRING);
				}

				_log.error("Expando Column" + columnName
						+ "Created Successfully.");
			}

		}
		return expandoTable;
	}
}
