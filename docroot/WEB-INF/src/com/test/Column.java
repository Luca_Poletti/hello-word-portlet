package com.test;


public class Column {
	private int col_ID;
	private String col_Name;
	private int col_Type;
	public Column (){
		this(19,"f",10);
	}
	public Column( int colID, String colName, int colType ){
		this.col_ID=colID;
		this.col_Name=colName;
		this.col_Type=colType;
		
	}
	/*
	public void add(int colID, String colName, int colType )
	{
	
		this.col_ID=colID;
		this.col_Name=colName;
		this.col_Type=colType;
	
	}*/
	public int getCol_ID() {
		return col_ID;
	}
	public String getCol_Name() {
		return col_Name;
	}
	public int getCol_Type() {
		return col_Type;
	}
	
	
}

