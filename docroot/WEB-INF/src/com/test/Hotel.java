package com.test;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import com.inkwell.internet.productregistration.model.PRHotel;
import com.inkwell.internet.productregistration.service.PRHotelLocalServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Hotel
 */
public class Hotel extends MVCPortlet {

	//add Hotel
	public void addHotel(ActionRequest actionRequest, ActionResponse actionResponse) 
	{
	try {
		

		//create Hotel persistance object
		PRHotel hotel= PRHotelLocalServiceUtil.createPRHotel(CounterLocalServiceUtil.increment());
		//fill the data in persistance object
		hotel.setHotelName(actionRequest.getParameter("hotel"));
		hotel.setAdress(actionRequest.getParameter("address"));
		hotel.setRooms(Long.parseLong(actionRequest.getParameter("rooms")));
		hotel.setCountry(actionRequest.getParameter("country"));
		
		//Add Hotel persistance object to database PRHotel table
		PRHotelLocalServiceUtil.addPRHotel(hotel);
   
		}
	catch(Exception e)	
	{
		System.out.println(e);
	}
	
	}
	
	//edit Hotel
	public void editHotel(ActionRequest actionRequest, ActionResponse actionResponse) 
	{
	try {
		
		
		System.out.println("EDIT" + actionRequest.getParameter("hotelId"));
   
		}
	catch(Exception e)	
	{
		System.out.println(e);
	}
	
	}

	//delete Hotel
	public void deleteHotel(ActionRequest actionRequest, ActionResponse actionResponse) 
	{
	try {
		
		//Delete Hotel persistance object to database PRHotel table
		PRHotelLocalServiceUtil.deletePRHotel(Long.parseLong(actionRequest.getParameter("hotelId")));	

   
		}
	catch(Exception e)	
	{
		System.out.println(e);
	}
	
	}
}
