package com.test;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;


/**
 * Portlet implementation class TestPortlet
 */
public class TestPortlet extends MVCPortlet {
	
	//Verifica utente
	public void chkControlla(ActionRequest actionRequest, ActionResponse actionResponse) 
	{
	try {
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId  = themeDisplay.getUser().getCompanyId();
 
        User utente = UserLocalServiceUtil.getUserByEmailAddress(companyId, actionRequest.getParameter("emailAddress"));
        
        PortletSession session = actionRequest.getPortletSession();
        session.setAttribute("utente", utente,PortletSession.APPLICATION_SCOPE);
       
/*		actionResponse.setRenderParameter("name", utente.getFirstName());
		actionResponse.setRenderParameter("cognome", utente.getLastName());	
		actionResponse.setRenderParameter("password", utente.getPassword());	
		actionResponse.setRenderParameter("email", utente.getEmailAddress());	*/
		actionResponse.setRenderParameter("azienda", "2");	
		
		System.out.print(utente);
		}
	catch(Exception e)	
	{
		System.out.println(e);
	}
	
	}
	

}
